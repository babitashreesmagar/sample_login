Cypress.Commands.add('load',() => {
    cy.intercept("GET","https://www.saucedemo.com/"). as ("loginpage");
    cy.visit("/");
    cy.wait("@loginpage");
})

Cypress.Commands.add('login',(username,password)=>{
    cy.get('#user-name').type(username);
    cy.get('#password').type(password);
    cy.get('#login-button').click();  
})

Cypress.Commands.add('blankLogin',(ele,credential)=>{  
    cy.get(`#${ele}`).type(credential);    
    cy.get('#login-button').click();  
})