let dashboard;
let invalidUsername;
let invalidPassword;
let blankUsernameMsg;
let blankPasswordMsg;
let alertMessage;
let tryAgainMsg;
const username = Cypress.env("username");
const password = Cypress.env("password");
describe("Login", () => {
  before(() => {
    cy.fixture("sample.json").then((data) => {
      dashboard = data.dashboard;
      invalidUsername = data.invalidUsername;
      invalidPassword = data.invalidPassword;
      blankUsernameMsg = data.blankusernamemsg;
      blankPasswordMsg = data.blankpasswordmsg;
      alertMessage = data.alertMessage;
      tryAgainMsg = data.tryAgainMsg;
    });
  });

  beforeEach(() => {
    cy.load();
  });

  //test case 01
  it.skip("[ Username ] field should have 'Username' as placeholder and [ Password ] field should have 'password' as placeholder.", () => {
    cy.get("#user-name").should("have.attr", "placeholder", "Username");
    cy.get("#password").should("have.attr", "placeholder", "Password");
  });

  //test case 02
  it.skip("The password should be masked", () => {
    cy.get("#password").should("have.attr", "type", "password");
  });

  //test case 03
  it("User should not be able to log in when both or one of the fields is left empty", () => {
    cy.blankLogin("user-name", username);
    cy.contains("h3[data-test='error']", blankPasswordMsg);

    cy.load();
    cy.blankLogin("password", password);
    cy.contains("h3[data-test='error']", blankUsernameMsg);

    cy.load();
    cy.get("#login-button").click();
    cy.contains("h3[data-test='error']", blankUsernameMsg);
  });

  //test case 04
  it.only("User should not be able to login with invalid credentials and the error message should collapse after user clicks on  [ X ]", () => {
    cy.login(invalidUsername, invalidPassword);
    cy.contains("h3[data-test='error']", alertMessage);
    cy.get('.error .error-button').click();
    cy.get("h3[data-test='error']").should('not.exist');
  });

  //test case 05
  //I just imagined this feature by myself
  it("User can attempt only 3 unsuccessful logins", () => {
    // Perform 3 unsuccessful login attempts
    for (let i = 0; i < 3; i++) {
      cy.login(invalidUsername, invalidPassword);
      cy.contains("h3[data-test='error']", alertMessage).should("be.visible");
      cy.load();
    }

    // Attempt the 4th login with invalid credentials
    cy.login(invalidUsername, invalidPassword);
    cy.contains("h3[data-test='error']", tryAgainMsg).should("be.visible");
  });

  //test case 06
  it("User should be able to login with valid credentials and with enter key", () => {
    cy.get("#user-name").type(username);
    cy.get("#password").type(password + "{enter}");
    cy.url().should("include", dashboard);
  });
});
